// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldTile.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "AI/Navigation/NavigationSystem.h"
#include "ActorPool.h"
#include "../InfiniteTerrainGameMode.h"


// Sets default values
AWorldTile::AWorldTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AWorldTile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	DestroyGeneratedEnvironmentActors();

	if (RequiresNavMesh)
	{
		if (!NavMeshBoundsVolume)
		{
			UE_LOG(LogTemp, Error, TEXT("[%s] No NavMeshBoundsVolume to check in to the pool!"));
			return;
		}

		NavMeshPool->CheckIn(NavMeshBoundsVolume);
	}
}

void AWorldTile::GenerateEnvironmentActors(TSubclassOf<AActor> ClassToSpawn, int32 MinNumber, int32 MaxNumber, float Radius, float MinScale, float MaxScale, bool CanYaw, bool CanPitch, bool CanRoll)
{
	int32 HowManyToSpawn = FMath::RandRange(MinNumber, MaxNumber);

	for (int32 i = 0; i < HowManyToSpawn; i++)
	{		
		// TODO Add CanSpawnPitched, CanSpawnYawed, and CanSpawnRolled parameters to the function
		FVector SpawnLocation;
		float SpawnScale = FMath::RandRange(MinScale, MaxScale);
		bool FoundFreeLocation = FindFreeLocation(SpawnLocation, Radius * SpawnScale);

		float Yaw = (CanYaw) ? FMath::RandRange(-180.0f, 180.0f) : 0.0f;
		float Pitch = (CanPitch) ? FMath::RandRange(-180.0f, 180.0f) : 0.0f;
		float Roll = (CanRoll) ? FMath::RandRange(-180.0f, 180.0f) : 0.0f;

		FRotator SpawnRotation = FRotator(Pitch, Yaw, Roll);

		if (FoundFreeLocation)
		{
			SpawnActor(ClassToSpawn, SpawnLocation, SpawnRotation, SpawnScale);
		}
	}
}

void AWorldTile::GenerateAI(TSubclassOf<APawn> PawnToSpawn, int32 MinNumber, int32 MaxNumber, float Radius)
{
	int32 HowManyToSpawn = FMath::RandRange(MinNumber, MaxNumber);

	for (int32 i = 0; i < HowManyToSpawn; i++)
	{
		// TODO Add CanSpawnPitched, CanSpawnYawed, and CanSpawnRolled parameters to the function
		FVector SpawnLocation;
		bool FoundFreeLocation = FindFreeLocation(SpawnLocation, Radius);

		FRotator SpawnRotation = FRotator(0.0f, FMath::RandRange(-180.0f, 180.0f), 0.0f);

		if (FoundFreeLocation)
		{
			SpawnActor(PawnToSpawn, SpawnLocation, SpawnRotation, 1.0f);
		}
	}
}

void AWorldTile::ConquerTile()
{
	if (!IsStartSquare && !IsConquered)
	{
		AInfiniteTerrainGameMode* GameMode = Cast<AInfiniteTerrainGameMode>(GetWorld()->GetAuthGameMode());

		if (!GameMode)
		{
			UE_LOG(LogTemp, Error, TEXT("[%s] Could not find the appropriate game mode!"));
			return;
		}

		GameMode->AddToScore(1);

		IsConquered = true;
	}
}

bool AWorldTile::FindFreeLocation(FVector& outFreeLocation, float Radius)
{
	FVector Min = FVector(0, -1900, 0);
	FVector Max = FVector(3950, 1900, 0);
	FBox Box = FBox(Min, Max);

	bool IsClearToSpawn = false;
	int32 Tries = 0;

	while (!IsClearToSpawn && Tries <= 10)
	{
		outFreeLocation = FMath::RandPointInBox(Box) + GetActorLocation();
		IsClearToSpawn = !SphereCast(outFreeLocation, Radius);
	}

	return IsClearToSpawn;
}

void AWorldTile::SpawnActor(TSubclassOf<AActor> ClassToSpawn, FVector Location, FRotator Rotation, float Scale)
{
	AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(ClassToSpawn, Location, Rotation);
	SpawnedActor->SetActorScale3D(FVector(Scale));

	APawn* SpawnedPawn = Cast<APawn>(SpawnedActor);

	if (SpawnedPawn)
	{
		SpawnedPawn->SpawnDefaultController();
	}

	SpawnedActor->AttachToActor(this, FAttachmentTransformRules(FAttachmentTransformRules::KeepWorldTransform));
}

bool AWorldTile::SphereCast(FVector Location, float Radius)
{
	FHitResult HitResult;
	bool HasHit = GetWorld()->SweepSingleByChannel(HitResult, Location, Location, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel2, FCollisionShape::MakeSphere(Radius));

	return HasHit;
}

void AWorldTile::DestroyGeneratedEnvironmentActors()
{
	TArray<AActor*> AttachedActors;
	GetAttachedActors(AttachedActors);

	for (int32 i = 0; i < AttachedActors.Num(); i++)
	{
		if (AttachedActors[i])
		{
			AttachedActors[i]->Destroy();
		}
	}
}

void AWorldTile::InitializeNavMeshPool(UActorPool* NavMeshPoolToSet)
{
	NavMeshPool = NavMeshPoolToSet;

	if (RequiresNavMesh)
	{
		NavMeshBoundsVolume = Cast<ANavMeshBoundsVolume>(NavMeshPool->CheckOut());

		if (!NavMeshBoundsVolume)
		{
			UE_LOG(LogTemp, Error, TEXT("[%s] NavMeshPool is empty!"), *(GetName()));
			return;
		}

		NavMeshBoundsVolume->SetActorLocation(GetActorLocation() + FVector(1950.0f, 0.0f, 0.0f));
		GetWorld()->GetNavigationSystem()->Build();
	}
}