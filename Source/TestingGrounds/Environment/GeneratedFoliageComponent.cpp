// Fill out your copyright notice in the Description page of Project Settings.

#include "GeneratedFoliageComponent.h"

void UGeneratedFoliageComponent::BeginPlay()
{
	Super::BeginPlay();

	SetCollisionProfileName(FName("NoCollision"));
	GenerateFoliage();
}

void UGeneratedFoliageComponent::GenerateFoliage()
{
	FVector Min = FVector(0, -1900, 0);
	FVector Max = FVector(3950, 1900, 0);
	FBox Bounds = FBox(Min, Max);

	int32 NumberToSpawn = FMath::RandRange(MinSpawnNumber, MaxSpawnNumber);

	for (int32 i = 0; i < NumberToSpawn; i++)
	{
		FVector SpawnLocation = FMath::RandPointInBox(Bounds);
		FRotator SpawnRotation = (CanSpawnYawed) ? FRotator(0.0f, FMath::RandRange(-180.0f, 180.0f), 0.0f) : FRotator(0.0f);
		float SpawnScale = (CanSpawnScaled) ? FMath::RandRange(MinScale, MaxScale) : 1.0f;

		SpawnFoliage(SpawnLocation, SpawnRotation, SpawnScale);
	}
}

void UGeneratedFoliageComponent::SpawnFoliage(FVector Location, FRotator Rotation, float Scale)
{
	FTransform SpawnTransform = FTransform(Rotation, Location, FVector(Scale));
	int32 SpawnedInstanceIndex = AddInstance(SpawnTransform);
}