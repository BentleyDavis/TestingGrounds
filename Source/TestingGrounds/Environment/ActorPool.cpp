// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorPool.h"


// Sets default values for this component's properties
UActorPool::UActorPool()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

AActor* UActorPool::CheckOut()
{
	AActor* ActorToCheckOut = nullptr;

	if (Pool.Num() != 0)
	{
		ActorToCheckOut = Pool.Pop();
	}

	return ActorToCheckOut;
}

void UActorPool::CheckIn(AActor* ActorToTurnIn)
{
	if (ActorToTurnIn)
	{
		Pool.Push(ActorToTurnIn);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("[%s] Tried to check in nullptr!"), *(GetName()));
	}
}