// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Weapons/BallProjectile.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/AnimInstance.h"
#include "Perception/AISense_Hearing.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	RootComponent = FP_Gun;

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	//VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void AWeapon::Fire()
{
	if (!FP_MuzzleLocation)
	{
		UE_LOG(LogTemp, Error, TEXT("An inheritor of AWeapon is missing a MuzzleLocation!"));
		return;
	}

	// try and fire a projectile
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator SpawnRotation = FP_MuzzleLocation->GetComponentRotation();

			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = FP_MuzzleLocation->GetComponentLocation();

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
			
			if (!ProjectileClass)
			{
				UE_LOG(LogTemp, Error, TEXT("Weapon does not have a ProjectileClass defined!"));
				return;
			}

			// spawn the projectile at the muzzle
			World->SpawnActor<ABallProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}

	// try and play the sound if specified
	if (!FireSound)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon is missing a fire sound!"));
		return;
	}

	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	UAISense_Hearing::ReportNoiseEvent(GetWorld(), FP_MuzzleLocation->GetComponentLocation(), 1.0f, Owner);

	if (!FPAnimInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon does not have an FPAnimInstance defined!"));
		return;
	}

	if (!TPAnimInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon does not have a TPAnimInstance defined!"));
		return;
	}

	if (!FPFireRifleAnimMontage)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon does not have an FPFireRifleAnimMontage defined!"));
		return;
	}

	if (!TPFireRifleAnimMontage)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon does not have a TPFireRifleAnimMontage defined!"));
		return;
	}

	FPAnimInstance->Montage_Play(FPFireRifleAnimMontage);
	TPAnimInstance->Montage_Play(TPFireRifleAnimMontage);
}