// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC_AI.h"
#include "Perception/AIPerceptionComponent.h"
#include "PatrollingGuard.h"

ANPC_AI::ANPC_AI()
{
	SetGenericTeamId(1);
}

void ANPC_AI::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		APatrollingGuard* ControlledPawn = Cast<APatrollingGuard>(InPawn);
		if (InPawn)
		{
			//ControlledPawn->OnDeath.AddDynamic(this, &ANPC_AI::KillPawn);
		}
	}
}

void ANPC_AI::KillPawn()
{
	APatrollingGuard* ControlledPawn = Cast<APatrollingGuard>(GetPawn());
	ControlledPawn->DetachFromControllerPendingDestroy();
}
