// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGroundsPlayerController.h"

ATestingGroundsPlayerController::ATestingGroundsPlayerController()
{
	SetGenericTeamId(0);
}

void ATestingGroundsPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

}

void ATestingGroundsPlayerController::KillPlayer()
{
	UE_LOG(LogTemp, Warning, TEXT("Killing player..."));
	return;
}
