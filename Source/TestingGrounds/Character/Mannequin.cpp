// Fill out your copyright notice in the Description page of Project Settings.

#include "Mannequin.h"
#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Weapons/Weapon.h"

// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(FName("CameraComp"));
	Camera->SetupAttachment(RootComponent);

	FPSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("FPSkeletalMeshComp"));
	FPSkeletalMesh->bOnlyOwnerSee = true;
	FPSkeletalMesh->SetupAttachment(Camera);
}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();

	if (!DefaultWeaponClass)
	{
		UE_LOG(LogTemp, Error, TEXT("Mannequin does not have a DefaultWeaponClass assigned!"));
		return;
	}

	Weapon = GetWorld()->SpawnActor<AWeapon>(DefaultWeaponClass, FVector(0), FRotator(0));
	Weapon->FPAnimInstance = FPSkeletalMesh->GetAnimInstance();
	Weapon->TPAnimInstance = GetMesh()->GetAnimInstance();
	Weapon->Owner = this;

	if (IsPlayerControlled())
	{
		Weapon->AttachToComponent(FPSkeletalMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
	}
	else
	{
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
	}

	if (InputComponent) // InputComponent is NULL if it is controlled by an AI
	{
		InputComponent->BindAction(FName("Fire"), EInputEvent::IE_Pressed, this, &AMannequin::PullTrigger);
	}
}

// Called every frame
void AMannequin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMannequin::PullTrigger()
{
	if (!Weapon)
	{
		UE_LOG(LogTemp, Error, TEXT("%s is missing a weapon!"), *(GetName()));
		return;
	}

	Weapon->Fire();
}

void AMannequin::UnPossessed()
{
	Super::UnPossessed();

	if (!Weapon) { return; }

	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
}

void AMannequin::ReparentWeapon(USkeletalMeshComponent* NewParentMesh, FName SocketName)
{
	Weapon->AttachToComponent(NewParentMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), SocketName);
}